CC = cc
SHELL = /bin/sh
CFLAGS += -Iinclude -Wall -Werror -Wextra -Wno-error=unused-result
PREFIX = /usr/local
DEBUGGER = lldb

NAME = cwpp

HEADER = src/enlapsed_time.h

SRCS:= src/linked_list/lstadd_back.c \
		src/linked_list/lstclear.c \
		src/linked_list/lstlast.c \
		src/linked_list/lstnew.c \
		src/linked_list/lstsize.c \
		src/linked_list/lsttostring.c \
		src/argument_parser.c \
		src/daemon.c \
		src/main.c \
		src/next.c \
		src/options_parser.c \
		src/pid.c


OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS)

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	find . -name "*.o" -delete -print

fclean: clean
	rm -f $(NAME)

re: fclean $(NAME)

install: $(NAME)
	cp $(NAME) $(PREFIX)/bin
	chmod +x $(PREFIX)/bin/$(NAME)

uninstall:
	rm $(PREFIX)/bin/$(NAME)

debug: CFLAGS += -g
debug: fclean $(NAME)
	$(DEBUGGER) $(NAME)

.PHONY: all clean fclean re install uninstall debug
