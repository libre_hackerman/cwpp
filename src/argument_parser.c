/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <argument_parser.h>


/*
 * Prints the help
 */
static void
display_help(arguments_t *args)
{
	int opt_width = 31;

	puts("Usage: cwpp [-n | -d DIRECTORY [-b] [-o FILE] [-O STRING] [-s NUMBER]] [-V] [-h]");
	puts("\nSet wallpapers in rotation");
	puts("\nOptions:");
	printf("%-*sRun in background\n", opt_width, "-b, --background");
	printf("%-*sChange to next wallpaper\n", opt_width, "-n, --next");
	printf("%-*sSet file with Feh options for particular wallpapers\n",
			opt_width, "-o, --options-file <file>");
	printf("%-*sSets default Feh options (default: %s)\n", opt_width,
			"-O, --default-option <string>", args->def_feh_options);
	printf("%-*sSets directory with the wallpapers\n", opt_width,
			"-d, --directory <directory>");
	printf("%-*sSets delay in seconds between wallpapers (default: %ds)\n",
			opt_width, "-s, --seconds <number>", args->secs_delay);
	printf("%-*sDisplays Feh commands and other extra information\n",
			opt_width, "-V, --verbose");
	printf("%-*sDisplay this help\n", opt_width, "-h, --help");
}


/*
 * Allocates and initializes an arguments_t with default values
 */
static arguments_t *
init_arguments()
{
	arguments_t *args = malloc(sizeof(arguments_t));

	args->next_mode = 0;
	args->background = 0;
	args->options_filename = NULL;
	args->def_feh_options = strdup("--bg-fill");
	args->walls_directory = NULL;
	args->secs_delay = 600;
	args->verbose = 0;

	return (args);
}


/*
 * Parses the command line arguments returning them in an arguments_t
 */
arguments_t *
parse_arguments(int argc, char *argv[])
{
	arguments_t *args = init_arguments();
	int op;

	struct option long_options[] = {
		{"background", no_argument, NULL, 'b'},
		{"next", no_argument, NULL, 'n'},
		{"options-file", required_argument, NULL, 'o'},
		{"default-option", required_argument, NULL, 'O'},
		{"directory", required_argument, NULL, 'd'},
		{"seconds", required_argument, NULL, 's'},
		{"verbose", no_argument, NULL, 'V'},
		{"help", no_argument, NULL, 'h'},
		{0, 0, 0, 0}
	};
	while ((op = getopt_long(argc, argv, ":bno:O:d:s:Vh",
					long_options, NULL)) != -1)
	{
		switch (op)
		{
			case 'b':
				args->background = 1;
				break;
			case 'n':
				args->next_mode = 1;
				break;
			case 'o':
				args->options_filename = strdup(optarg);
				break;
			case 'O':
				free(args->def_feh_options);
				args->def_feh_options = strdup(optarg);
				break;
			case 'd':
				args->walls_directory = strdup(optarg);
				break;
			case 's':
				if (isdigit(*optarg))
					args->secs_delay = atoi(optarg);
				else
				{
					fprintf(stderr, "\"%s\" is not a valid number of seconds\n",
							optarg);
					free_arguments(args);
					exit(1);
				}
				break;
			case 'V':
				args->verbose = 1;
				break;
			case 'h':
				display_help(args);
				free_arguments(args);
				exit(0);
				break;
			case ':':
				fprintf(stderr, "Missing required argument\n");
				free_arguments(args);
				exit(1);
				break;
			case '?':
				fprintf(stderr, "Unknown argument\n");
				free_arguments(args);
				exit(1);
				break;
		}
	}

	return (args);
}


/*
 * Deallocates an arguments_t
 */
void
free_arguments(arguments_t *args)
{
	if (args)
	{
		if (args->options_filename)
			free(args->options_filename);
		if (args->def_feh_options)
			free(args->def_feh_options);
		if (args->walls_directory)
			free(args->walls_directory);
		free(args);
	}
}
