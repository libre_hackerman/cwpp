/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <signal.h>
#include <cwpp.h>

/*
 * Change to the next wallpaper by sending SIGUSR1 to the CWPP daemon
 */
void
go_next()
{
	int pid = get_cwpp_pid();

	if (pid == -1)
	{
		fprintf(stderr, "No CWPP process found\n");
		return ;
	}
	if (kill(pid, SIGUSR1))
		err(1, "Could not contact CWPP daemon (%d)", pid);
}
