/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linked_list.h>
#include <stdlib.h>

static void
del_nodes(list_t *node, void (*del)(void*))
{
	if (node->next)
		del_nodes(node->next, del);
	if (del)
		del(node->content);
	free(node);
}

void
lstclear(list_t **lst, void (*del)(void*))
{
	del_nodes(*lst, del);
	*lst = NULL;
}
