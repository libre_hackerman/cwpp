/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <err.h>
#include <unistd.h>
#include <cwpp.h>


/*
 * Returns boolean confirming if pid is a CWPP process
 */
static int
is_proc_cwpp(char *pid)
{
	char *path;
	FILE *comm;
	char readed[5];
	int is_proc_cwpp = 0;

	/*
	 * path = "/proc/1234/comm" being 1234 the PID
	 * 11 => strlen("/proc/") + strlen("/comm")
	 */
	path = calloc(1, 11 + strlen(pid) + 1);
	strcat(path, "/proc/");
	strcat(path, pid);
	strcat(path, "/comm");

	if ((comm = fopen(path, "r")) == NULL)
	{
		warn("%s", path);
		free(path);
		return (0);
	}

	/* 5 => strlen("cwpp\n") */
	if (fread(readed, 1, 5, comm) == 5 && strcmp(readed, "cwpp\n") == 0)
		is_proc_cwpp = 1;
	fclose(comm);
	free(path);

	return (is_proc_cwpp);
}


/*
 * Returns the PID of the CWPP process, or -1 if is not loaded
 */
int
get_cwpp_pid()
{
	DIR *proc;
	struct dirent *ent;
	int aux_pid, pid = -1;

	if ((proc = opendir("/proc")) == NULL)
	{
		warn("/proc");
		return (-1);
	}

	while ((ent = readdir(proc)) != NULL)
	{
		if (isdigit(*(ent->d_name)) && is_proc_cwpp(ent->d_name))
		{
			/* If it's not *this* process */
			if ((aux_pid = atoi(ent->d_name)) != getpid())
			{
				pid = aux_pid;
				break;
			}
		}
	}
	closedir(proc);

	return (pid);
}
