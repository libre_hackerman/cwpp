/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include <err.h>
#include <linked_list.h>
#include <options_parser.h>

/*
 * Duplicates a char in the heap
 */
static char *
chardup(char c)
{
	char *nc = malloc(1);
	*nc = c;
	return (nc);
}


/*
 * Converts line to char*, adds it to lines if matches regex and clears
 * the original line list
 */
static void
add_line(list_t **lines, list_t **line, regex_t *regex)
{
	char *str_line = lsttostring(*line);

	if (!regexec(regex, str_line, 0, NULL, 0))
		lstadd_back(lines, lstnew(str_line));
	else
	{
		fprintf(stderr, "Incorrect line: \"%s\"\n", str_line);
		free(str_line);
	}
	lstclear(line, free);
}


/*
 * Reads the specified path and returns a list_t of char* with the lines
 * that match the ".+ : .+" regex pattern
 */
static list_t *
readlines(char *filename)
{
	FILE *file;
	list_t *lines, *line;
	char buffer;
	regex_t regex;

	if ((file = fopen(filename, "r")) == NULL)
	{
		warn("Error opening %s", filename);
		return (NULL);
	}

	lines = NULL;
	line = NULL;
	regcomp(&regex, ".+ : .+", REG_EXTENDED);
	while (fread(&buffer, 1, 1, file))
	{
		if (buffer != '\n')
			lstadd_back(&line, lstnew(chardup(buffer)));
		else
			add_line(&lines, &line, &regex);
	}
	if (line)
		add_line(&lines, &line, &regex);
	fclose(file);
	regfree(&regex);

	return (lines);
}


/*
 * Reads the specified file and returns a NULL terminated array with the
 * feh options parsed
 */
feh_options_t *
parse_options(char *filename)
{
	list_t *lines, *aux;
	feh_options_t *ops_array;
	int i, width;
	char *raw_line, *name, *options;

	if ((lines = readlines(filename)) == NULL)
		return (NULL);
	aux = lines;
	ops_array = calloc(lstsize(lines) + 1, sizeof(feh_options_t));
	i = 0;
	while (aux)
	{
		raw_line = (char*)aux->content;

		/* Parsing of the filename */
		/* The -1 is because of the space before the colon */
		width = strchr(raw_line, ':') - raw_line - 1;
		name = malloc(width + 1);
		strncpy(name, raw_line, width);
		name[width] = '\0';

		/* Parsing of the feh options */
		/* The +2 is to skip the colon and the space */
		width = strlen(strchr(raw_line, ':') + 2);
		options = malloc(width + 1);
		strcpy(options, strchr(raw_line, ':') + 2);

		ops_array[i].filename = name;
		ops_array[i++].options = options;
		aux = aux->next;
	}
	lstclear(&lines, free);

	return (ops_array);
}


/*
 * Frees the NULL terminated array of feh options and its contents
 */
void
free_options(feh_options_t *ops_array)
{
	if (!ops_array)
		return ;

	for (int i = 0; ops_array[i].filename && ops_array[i].options; i++)
	{
		free(ops_array[i].filename);
		free(ops_array[i].options);
	}
	free(ops_array);
}
