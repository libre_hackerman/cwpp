/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <err.h>
#include <signal.h>
#include <options_parser.h>
#include <cwpp.h>

/*
 * This bool is used as condition of the loop that keeps the
 * daemon alive
 */
int keep_walls_loop = 1;


/*
 * This bool will be updated with the arguments data. Its purpose is to
 * let signal handlers know if display extra information
 */
int verbose = 0;


/*
 * SIGUSR1 callback. Does the same as me in PHP class.
 * Its purpose is to exit the sleep function. Displays informative
 * message if verbose flag is enabled
 */
static void
change_wallpaper(int sig)
{
	(void)sig;  /* Supress unused warning */

	if (verbose)
		puts("Manually switching to next wallpaper (cwpp -n)");
}


/*
 * Exit signals callback. It will exit the sleep function and change
 * keep_walls_loop to 0
 */
static void
exit_loop(int sig)
{
	fprintf(stderr, "Received signal %d: exiting...\n", sig);
	keep_walls_loop = 0;
}


/*
 * Returns a lowercase copy of a string
 */
static char *
str_to_lower(char *str)
{
	size_t len = strlen(str);
	char *lower = calloc(1, len + 1);

	for (size_t i = 0; i < len; i++)
		lower[i] = tolower(str[i]);

	return (lower);
}


/*
 * This function returns 0 if the filename doesn't match the extension
 * provided
 */
static int
check_extension(char *filename, char *extension)
{
	char *fn_ext;
	int match;

	if ((fn_ext = strrchr(filename, '.')) != NULL)
		fn_ext++;
	else
		return (0);

	fn_ext = str_to_lower(fn_ext);
	extension = str_to_lower(extension);
	match = !strcmp(fn_ext, extension);
	free(fn_ext);
	free(extension);

	return (match);
}


/*
 * Returns a NULL terminated array of the filenames of the wallpapers
 * in the especified directory
 */
static char **
get_walls_filenames(char *directory_name)
{
	DIR *dir;
	struct dirent *ent;
	char **walls;
	size_t i;

	if (!directory_name)
	{
		fprintf(stderr, "No wallpapers directory provided\n");
		return (NULL);
	}

	if ((dir = opendir(directory_name)) == NULL)
	{
		warn("%s", directory_name);
		return (NULL);
	}

	/* Count the number of wallpapers */
	i = 0;
	while ((ent = readdir(dir)) != NULL)
	{
		if (check_extension(ent->d_name, "png") ||
				check_extension(ent->d_name, "jpg") ||
				check_extension(ent->d_name, "jpe") ||
				check_extension(ent->d_name, "bmp"))
			i++;
	}
	walls = calloc(sizeof(char*), i + 1);

	/* Save the wallpapers in the array */
	rewinddir(dir);
	i = 0;
	while ((ent = readdir(dir)) != NULL)
	{
		if (check_extension(ent->d_name, "png") ||
				check_extension(ent->d_name, "jpg") ||
				check_extension(ent->d_name, "jpe") ||
				check_extension(ent->d_name, "bmp"))
			walls[i++] = strdup(ent->d_name);
	}
	closedir(dir);

	return(walls);
}


/*
 * Shuffle a NULL terminated strings array
 */
static void
shuffle(char **array)
{
	size_t rnd, len = 0;
	char *aux;

	while (array[len])
		len++;

	for (size_t i = 0; i < len; i++)
	{
		rnd = rand() % len;
		aux = array[rnd];
		array[rnd] = array[i];
		array[i] = aux;
	}
}


/*
 * Checks if there are special feh options for the wallpaper filename and
 * returns them. If not, returns the especified default
 */
static char *
check_custom_options(char *filename, feh_options_t *ops, char *def_options)
{
	if (!ops)
		return (def_options);

	for (feh_options_t *op = ops; op->filename; op++)
	{
		if (strcmp(filename, op->filename) == 0)
			return (op->options);
	}

	return (def_options);
}


/*
 * Starts the wallpaper changing daemon acording the passed arguments
 */
void
go_daemon(arguments_t *args)
{
	char **walls;
	char *feh_options, *feh_cmdline;
	feh_options_t *custom_options;

	verbose = args->verbose;

	if (get_cwpp_pid() != -1)
	{
		fprintf(stderr, "CWPP is already running (PID %d)\n", get_cwpp_pid());
		free_arguments(args);
		exit(1);
	}

	/* Check Feh is available */
	if (system("feh -v") == -1)
	{
		fprintf(stderr, "Feh is not in $PATH\n");
		free_arguments(args);
		exit(1);
	}

	/* Load wallpapers filenames */
	if ((walls = get_walls_filenames(args->walls_directory)) == NULL)
	{
		free_arguments(args);
		exit(1);
	}

	/* Load Feh options */
	if (args->options_filename)
	{
		if ((custom_options = parse_options(args->options_filename)) == NULL)
		{
			free_arguments(args);
			exit(1);
		}
	}
	else
		custom_options = NULL;

	/* Daemonize */
	if (args->background)
		daemon(1, 1);

	/* Wallpaper changer loop */
	srand(time(NULL));
	shuffle(walls);
	signal(SIGUSR1, change_wallpaper);
	signal(SIGTERM, exit_loop);
	signal(SIGINT, exit_loop);
	while(keep_walls_loop)
	{
		for (char **cur_wall = walls; *cur_wall && keep_walls_loop; cur_wall++)
		{
			/* Check custom Feh options */
			feh_options = check_custom_options(*cur_wall, custom_options,
					args->def_feh_options);

			/*
			 * 18 = "feh --no-fehbg " + [space betw. options and wall path] +
			 *    ["/" betw. directory and filename + ending null byte
			 */
			feh_cmdline = calloc(1, 18 + strlen(feh_options) +
					strlen(args->walls_directory) + strlen(*cur_wall));
			sprintf(feh_cmdline, "feh --no-fehbg %s %s/%s", feh_options,
					args->walls_directory, *cur_wall);
			if (args->verbose)
				puts(feh_cmdline);
			system(feh_cmdline);
			free(feh_cmdline);

			sleep(args->secs_delay);
		}
	}

	/* Free the kraken */
	free_arguments(args);
	free_options(custom_options);
	for (char **cur_wall = walls; *cur_wall; cur_wall++)
		free(*cur_wall);
	free(walls);
}
