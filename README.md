# cwpp

CWPP is a wallpaper changer written in C that relies in Feh. Oriented to WM users that want to set a rotation of wallpapers.

The syntax of the special options file is the name of the picture, a space, a colon, a space and the options to pass to Feh. Example:
```
picture1.png : --bg-fill --geometry +0-30
picture2.png : --bg-max
pictureN.jpg : --option1 --option2 --optionN
```

Usage:
```
Usage: cwpp [-n | -d DIRECTORY [-b] [-o FILE] [-O STRING] [-s NUMBER]] [-h]

Set wallpapers in rotation

Options:
-b, --background               Run in background
-n, --next                     Change to next wallpaper
-o, --options-file <file>      Set file with Feh options for particular wallpapers
-O, --default-option <string>  Sets default Feh options
-d, --directory <directory>    Sets directory with the wallpapers
-s, --seconds <number>         Sets delay in seconds between wallpapers
-h, --help                     Display this help

```
