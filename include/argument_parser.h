/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ARGUMENT_PARSER_H
#define ARGUMENT_PARSER_H

typedef struct arguments_s
{
	int next_mode;
	int background;
	char *options_filename;
	char *def_feh_options;
	char *walls_directory;
	int secs_delay;
	int verbose;
}
arguments_t;


arguments_t *
parse_arguments(int argc, char *argv[]);

void
free_arguments(arguments_t *args);

#endif /* ARGUMENT_PARSER_H */
