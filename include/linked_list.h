/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct list_s
{
	void *content;
	struct list_s *next;
}
list_t;


list_t *
lstnew(void *content);

void
lstadd_back(list_t **alst, list_t *new);

void
lstclear(list_t **lst, void (*del)(void*));

list_t *
lstlast(list_t *lst);

int
lstsize(list_t *lst);

char *
lsttostring(list_t *lst);

#endif /* LINKED_LIST_H */
